#ifndef _MINIMAX_HIDDEN_H
#define _MINIMAX_HIDDEN_H

#include "minimax.h"
#include "my_defines.h"

typedef struct __minimax
{
    p_euristica_t euristica;
    p_inserisci_pedina_t inserisci_pedina;
    p_rimuovi_pedina_t rimuovi_pedina;
    p_vittoria_t vittoria;
    my_int n_mosse, punteggio_vittoria, punteggio_perdita;
} _minimax;

typedef _minimax* p_minimax;

#endif
