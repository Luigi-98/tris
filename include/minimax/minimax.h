#ifndef _MINIMAX_H
#define _MINIMAX_H

#include "my_defines.h"

// dato uno stato e il giocatore, torna il valore dell'euristica a
//  favore del giocatore nello stato dato
typedef my_int (*p_euristica_t)(pvoid pstato, my_int giocatore);
// dato uno stato, verifica se ci sono state vittorie e torna:
//  - 0 nel caso di nessuna vittoria;
//  - il giocatore che ha vinto altrimenti.
typedef my_int (*p_vittoria_t)(pvoid pstato);
typedef my_int (*p_inserisci_pedina_t)(pvoid pstato, my_int giocatore, my_int mossa);
typedef void (*p_rimuovi_pedina_t)(pvoid pstato, my_int giocatore, my_int mossa);

/*
 Stima la migliore mossa da fare sulla base dell'algoritmo minimax e
        della situazione attuale; ritorna il numero della colonna nella
        quale mettere la pedina

 pminimax_:      istanza dell'oggetto minimax
 psituazione:   puntatore alla struct che contiene le informazioni sulla
                situazione a partire dalla quale stimare la migliore mossa
 depth:         profondità alla quale deve fermarsi l'algoritmo mimimax
 pscore:        puntatore alla variabile nella quale salvare il punteggio
                della mossa migliore da fare
*/
my_int start_minimax(pvoid pminimax_, pvoid psituazione, my_int depth, pmy_int pscore, my_int prossimo_giocatore);

/*
 Inizializza le funzioni legate al gioco
 
 pminimax_:             istanza dell'oggetto minimax
 p_euristica_:          dà stima della bontà della situazione di gioco
 p_inserisci_pedina_:   fa una mossa
 p_rimuovi_pedina:      annulla quella mossa
*/
pvoid minimax_init(p_euristica_t euristica, p_inserisci_pedina_t inserisci_pedina, p_rimuovi_pedina_t rimuovi_pedina, p_vittoria_t vittoria, my_int n_mosse, my_int punteggio_vittoria, my_int punteggio_perdita);

void minimax_free(pvoid p_my_minimax);

#endif
