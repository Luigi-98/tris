#include "my_defines.h"
#include "minimax.h"
#include "minimax.hidden.h"

#include <limits.h>
#include <stdlib.h>

#include <stdio.h>

/*
 * E' il minimax vero e proprio: ci serve per fare meno accessi a puntatori.
 * 
 * Impongo depth>=1 (perché già da start_minimax ho gestito il caso depth==0),
 * così a depth==1 confronto direttamente le euristiche dei figli
 * 
 * */
my_int minimax_internal(p_minimax p_my_minimax, pvoid psituazione, my_int depth, my_int current_depth, my_int prossimo_giocatore, my_int giocatore_partenza, pmy_int pmossa)
{
    my_int mossa, mossa_best=-1, punteggio_best, punteggio, vittoria, pedina_inserita;
    
    // siamo in fase max (o in fase min)?
    my_int usemax=(prossimo_giocatore==giocatore_partenza);
    
    punteggio_best=usemax?INT_MIN:INT_MAX;
    
    // genero le prossime possibili mosse
    for (mossa=0; mossa<p_my_minimax->n_mosse; mossa++)
    {
        // faccio la mossa
        pedina_inserita = p_my_minimax->inserisci_pedina(psituazione, prossimo_giocatore, mossa);
        
        // se non ho potuto fare questa mossa, passo alla successiva
        if (!pedina_inserita) continue;
        
        //tris_print(psituazione);
        
        vittoria = p_my_minimax->vittoria(psituazione);
        
        // se il nodo è terminale perché c'è una vittoria o perché sono
        //  arrivato al fondo       ==> euristica
        //
        // se no
        //                          ==> max/min punteggi
        if (vittoria) // qualcuno ha vinto
        {
            punteggio = ((vittoria==giocatore_partenza)?
                p_my_minimax->punteggio_vittoria:
                p_my_minimax->punteggio_perdita) / current_depth;
        }
        else if (depth==1) // nodo terminale
        {
            punteggio = p_my_minimax->euristica(psituazione, giocatore_partenza);
        }
        else // ci sono figli
        {
            punteggio = minimax_internal(p_my_minimax, psituazione, depth-1, current_depth+1, prossimo_giocatore^3, giocatore_partenza, NULL);
        }
        
        // annullo la mossa
        p_my_minimax->rimuovi_pedina(psituazione, prossimo_giocatore, mossa);
        
        if (usemax)
        {
            if (punteggio>=punteggio_best)
            {
                mossa_best=mossa;
                punteggio_best=punteggio;
            }
        }
        else
        {
            if (punteggio<=punteggio_best)
            {
                mossa_best=mossa;
                punteggio_best=punteggio;
            }
        }
    }
    
    if (mossa_best==-1) // non è stato possibile inserire pedine (nodo terminale)
    {
        punteggio_best = p_my_minimax->euristica(psituazione, giocatore_partenza);
    }
    
    if (pmossa!=NULL)
    {
        *pmossa=mossa_best;
    }
    
    return punteggio_best;
}


/*
 Stima la migliore mossa da fare sulla base dell'algoritmo minimax e
        della situazione attuale; ritorna il numero della colonna nella
        quale mettere la pedina

 pminimax_:      istanza dell'oggetto minimax
 psituazione:   puntatore alla struct che contiene le informazioni sulla
                situazione a partire dalla quale stimare la migliore mossa
 depth:         profondità alla quale deve fermarsi l'algoritmo mimimax
 pscore:        puntatore alla variabile nella quale salvare il punteggio
                della mossa migliore da fare
*/
my_int start_minimax(pvoid pminimax_, pvoid psituazione, my_int depth, pmy_int pscore, my_int prossimo_giocatore)
{
    my_int vittoria;
    my_int mossa;
    p_minimax p_my_minimax=(p_minimax)pminimax_;
    
    if ((vittoria=(p_my_minimax->vittoria(psituazione))))
    {
        *pscore=(vittoria==prossimo_giocatore)?
                p_my_minimax->punteggio_vittoria:
                p_my_minimax->punteggio_perdita;
        return -1;
    }
    if ((!depth))
    {
        *pscore=p_my_minimax->euristica(psituazione, prossimo_giocatore);
        return -1;
    }
    *pscore=minimax_internal(p_my_minimax, psituazione, depth, 1, prossimo_giocatore, prossimo_giocatore, &mossa);
    return mossa;
}


pvoid minimax_init(p_euristica_t euristica, p_inserisci_pedina_t inserisci_pedina, p_rimuovi_pedina_t rimuovi_pedina, p_vittoria_t vittoria, my_int n_mosse, my_int punteggio_vittoria, my_int punteggio_perdita)
{
    _minimax _tmp_my_minimax = {euristica = euristica,
                                inserisci_pedina = inserisci_pedina,
                                rimuovi_pedina = rimuovi_pedina,
                                vittoria = vittoria,
                                n_mosse = n_mosse,
                                punteggio_vittoria = punteggio_vittoria,
                                punteggio_perdita = punteggio_perdita};
    p_minimax p_my_minimax = (p_minimax) malloc(sizeof(_minimax));
    if (p_my_minimax==NULL) return p_my_minimax;
    
    *p_my_minimax=_tmp_my_minimax;
    
    return (pvoid)p_my_minimax;
}

void minimax_free(pvoid p_my_minimax)
{
    free(p_my_minimax);
}
