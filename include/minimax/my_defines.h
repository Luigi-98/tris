#ifndef _MY_DEFINES_MINIMAX
#define _MY_DEFINES_MINIMAX

typedef void*          pvoid;
typedef int*           pint;

typedef int            my_int;
typedef my_int*        pmy_int;

typedef unsigned       my_unsigned;
typedef my_unsigned*   pmy_unsigned;

typedef float          my_float;
typedef my_float*      pmy_float;

typedef double         my_double;
typedef my_double*     pmy_double;

#endif
