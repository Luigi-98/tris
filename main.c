#include "tris/tris.h"

#include <stdio.h>
#include <limits.h>
#include <stdlib.h>

int main(void)
{
    pvoid p_my_tris = tris_init();

    tris_interface_init(p_my_tris);
    
    tris_clear(p_my_tris);
    
    int mossa, score, vincente;
    
    pvoid pminimax=minimax_init(tris_euristica, tris_inserisci_pedina, tris_rimuovi_pedina, tris_vittoria, 9, INT_MAX, INT_MIN);
    
    tris_print(p_my_tris);
    
    do
    {
        /*printf("Inserisci mossa (1-9): ");
        scanf("%d", &mossa);
        printf("\n");*/
        mossa=tris_ask_mossa(p_my_tris, "Inserisci mossa (1-9): ");
        if (tris_inserisci_pedina(p_my_tris, 1, mossa-1))
        {
            tris_inserisci_pedina(p_my_tris, 2, start_minimax(pminimax, p_my_tris, 10, &score, 2));
            //printf("Score della mossa dell'avversario: %d\n", score);
        }
        else
        {
            //printf("Mossa non valida!");
        }

        tris_print(p_my_tris);
        //printf("\n");
    } while (!(vincente=tris_vittoria(p_my_tris)) && !tris_full(p_my_tris));
    
    if (!vincente)
    {
        tris_end_match(p_my_tris, 0);
    }
    else
    {
        tris_end_match(p_my_tris, (vincente==1)?1:-1);
    }

    tris_free(p_my_tris);
    
    return EXIT_SUCCESS;
}
