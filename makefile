SHELL=/bin/sh

CC=gcc
CFLAGS = -std=c99 -Wall -Wextra -lncurses

OUTFOLDER=.
MAINFOLDER=$(OUTFOLDER)
TRIS=$(MAINFOLDER)/tris
INCLUDES=$(MAINFOLDER)/include
MINIMAX=$(INCLUDES)/minimax

$(info )
$(info **** STARTING ... ****)
$(info )
$(info )

$(OUTFOLDER)/main: $(TRIS)/tris.o $(MAINFOLDER)/main.c
	$(info )
	$(info **** Compiling main... ****)
	$(info )
	$(info )
	$(CC) $(MAINFOLDER)/main.c $(TRIS)/tris.o $(MINIMAX)/minimax.o -o $(OUTFOLDER)/main $(CFLAGS)

$(TRIS)/tris.o: $(TRIS)/tris.c $(TRIS)/tris.h $(MINIMAX)/minimax.o
	$(info )
	$(info **** Compiling tris... ****)
	$(info )
	$(info )
	$(CC) -c $(TRIS)/tris.c -o $(TRIS)/tris.o $(CFLAGS)

$(MINIMAX)/minimax.o: $(MINIMAX)/minimax.c $(MINIMAX)/minimax.hidden.h $(MINIMAX)/minimax.h
	$(info )
	$(info **** Compiling minimax... ****)
	$(info )
	$(info )
	$(CC) -c $(MINIMAX)/minimax.c -o $(MINIMAX)/minimax.o $(CFLAGS)

clear-objects:
	rm -f $(MINIMAX)/minimax.o
	rm -f $(TRIS)/tris.o
	rm -f $(OUTFOLDER)/main

all: clear-objects $(OUTFOLDER)/main
