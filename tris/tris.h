#ifndef _TRIS_H
#define _TRIS_H

#include "../include/minimax/minimax.h"

#include <ncurses.h>

typedef void* pvoid;
typedef int* pint;
typedef pint* ppint;

typedef struct _tris
{
    int pscacchiera[9];
    int n_elem;
    WINDOW *pwin, *pwinres;
} tris;

typedef tris* ptris;

pvoid tris_init();
void tris_free(pvoid pmytris_);
void tris_interface_init(pvoid pmytris_);
void tris_clear(pvoid pstato);
void tris_fill(pvoid pstato, int n);
int tris_inserisci_pedina(pvoid pstato, int giocatore, int mossa);
int tris_vittoria(pvoid pmytris);
int tris_euristica(pvoid pstato, int giocatore);
void tris_rimuovi_pedina(pvoid pstato, int giocatore, int mossa);
int tris_full(pvoid pstato);

int tris_print(pvoid pstato);
int tris_ask_mossa(pvoid pmytris_, char* txt);
int tris_end_match(pvoid pstato, int res);

#endif
