#include "tris.h"

#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

#include <ncurses.h>
#include <string.h>

pvoid tris_init()
{
    ptris pres = (ptris)malloc(sizeof(tris));
    if (pres==NULL) return (pvoid)pres;

    pres->pwin=NULL;
    return (pvoid)pres;
}

void tris_interface_init(pvoid pmytris_)
{
    ptris pmytris=(ptris)pmytris_;

    initscr();
    assume_default_colors(-1,-1);
    start_color();
    
    noecho();
    raw();
    cbreak();
    curs_set(0);

    int x,y;
    getmaxyx(stdscr, y, x);

    pmytris->pwin=newwin(5,9,y/2-3,x/2-5);
    pmytris->pwinres=newwin(1,x,y/2, 0);
    //mvprintw(0,0,"%s\n",has_colors()?"Has colors":"Has no colors");
    refresh();

    box(pmytris->pwin, 0, 0);
    mvwprintw(pmytris->pwin, 0, 1, "Tris");
    wrefresh(pmytris->pwin);

}

void tris_free(pvoid pmytris_)
{
    ptris pmytris = (ptris)pmytris_;
    if (pmytris->pwin!=NULL)
    {
        getch();
        use_default_colors();
        endwin();
    }
    free(pmytris);
}

int tris_vittoria(pvoid pmytris)
{
    pint pscacchiera=((ptris)pmytris)->pscacchiera;
    
    int check, i, j;
    
    // controllo le righe
    for (i=0; i<3; i++) // ciclo sulle righe
    {
        check=pscacchiera[i*3+0];
        check&=pscacchiera[i*3+1];
        check&=pscacchiera[i*3+2];
        if (check) return check;
    }
    
    // controllo le colonne
    for (j=0; j<3; j++) // ciclo sulle colonne
    {
        check=pscacchiera[0*3+j];
        check&=pscacchiera[1*3+j];
        check&=pscacchiera[2*3+j];
        if (check) return check;
    }
    
    // controllo la prima diagonale (\)
    check=pscacchiera[0*3+0];
    check&=pscacchiera[1*3+1];
    check&=pscacchiera[2*3+2];
    if (check) return check;
    
    // controllo la seconda diagonale (/)
    check=pscacchiera[0*3+2];
    check&=pscacchiera[1*3+1];
    check&=pscacchiera[2*3+0];
    if (check) return check;
    
    return 0;
}

int tris_euristica(pvoid pstato, int giocatore)
{
    return INT_MIN;
}

int tris_print_cell(pvoid pstato, int i, int j)
{
    ptris p_my_stato=(ptris)pstato;
    WINDOW* pwin = p_my_stato->pwin;
    char giocatori[] = {'_', 'O', 'X'};
    wmove(pwin, i+1, j*2+2);
    return wprintw(pwin, "%c", giocatori[p_my_stato->pscacchiera[i*3+j]]);
}

int tris_print_centered(WINDOW* pwin, char* txt, int width)
{
    int len = strlen(txt), count;

    wmove(pwin, getmaxy(pwin)/2, (getmaxx(pwin)-width)/2);
    
    for (count=0; count<(width-len)/2; count++)
        wprintw(pwin, " ");

    wprintw(pwin, "%s", txt);

    for (count=0; count<(width-len)/2; count++)
        wprintw(pwin, " ");

    return 0;
}

// res = 1,0,-1 (vinto, pareggio, perso)
int tris_end_match(pvoid pstato, int res)
{
    WINDOW* pwin = ((ptris)pstato)->pwinres;
    wattron(pwin, A_STANDOUT);
    switch (res)
    {
        case 1:
            tris_print_centered(pwin, "Hai vinto!", 15);
            break;
        case -1:
            tris_print_centered(pwin, "Ho vinto!", 15);
            break;
        case 0:
            tris_print_centered(pwin, "Abbiamo perso entrambi!", 30);
            break;
    }
    wattroff(pwin, A_STANDOUT);
    wrefresh(pwin);
    refresh();
    return 0;
}

int tris_print(pvoid pstato)
{
    int i,j;
    
    for (i=0; i<3; i++)
    {
        for (j=0; j<3; j++)
        {
            tris_print_cell(pstato, i, j);
        }
        //wprintw(pwin, "\n");
    }
    
    return 0;
}

int tris_ask_mossa(pvoid pmytris_, char* txt)
{
    ptris pmytris=(ptris)pmytris_;
    WINDOW* pwin = pmytris->pwin;
    int res;

    /*wmove(pwin, 0, 1);
    wprintw(pwin, "%s", txt);
    wscanw(pwin, "%d", &res);*/

    int i=0, j=0;
    int c;

    keypad(pwin, true);
    
    do
    {
        tris_print(pmytris_);
        wattron(pwin, A_STANDOUT);
        tris_print_cell(pmytris_, i, j);
        wattroff(pwin, A_STANDOUT);
        wrefresh(pwin);
        
        wmove(pwin, 0, 0);
        c=wgetch(pwin);

        switch (c)
        {
            case KEY_RIGHT:
                j += (j<2)?1:0;
                break;
            case KEY_LEFT:
                j -= (j>0)?1:0;
                break;
            case KEY_DOWN:
                i += (i<2)?1:0;
                break;
            case KEY_UP:
                i -= (i>0)?1:0;
                break;
        }
        refresh();
    } while (c!=KEY_ENTER && c!='\n');

    return i*3+j+1;
}

int tris_inserisci_pedina(pvoid pstato, int giocatore, int mossa)
{
    pint pscacchiera = ((ptris)pstato)->pscacchiera;
    int riga = mossa/3, colonna = mossa%3;
    
    if (pscacchiera[riga*3+colonna] || mossa<0)
        return 0;
    
    ((ptris)pstato)->n_elem++;
    
    return pscacchiera[riga*3+colonna]=giocatore;
}

void tris_rimuovi_pedina(pvoid pstato, int giocatore, int mossa)
{
    pint pscacchiera = ((ptris)pstato)->pscacchiera;
    int riga = mossa/3, colonna = mossa%3;
    
    pscacchiera[riga*3+colonna]=0;
    ((ptris)pstato)->n_elem--;
}

void tris_fill(pvoid pstato, int n)
{
    pint pscacchiera = ((ptris)pstato)->pscacchiera;
    int i,j;
    
    for (i=0; i<3; i++)
    {
        for (j=0; j<3; j++)
        {
            pscacchiera[i*3+j]=n;
        }
    }
    
    return;
}

void tris_clear(pvoid pstato)
{
    ((ptris)pstato)->n_elem=0;
}

int tris_full(pvoid pstato)
{
    return ((ptris)pstato)->n_elem == 9;
}
